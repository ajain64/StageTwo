import urllib2
from bs4 import BeautifulSoup
import time
import glob
import os
import sys
import codecs
from random import *

IMPLICIT_WAIT_TIME = "30000"

PAGE_LOAD_TIMEOUT_SEC = 30

PER_PAGE_TRANSITION_PRE_SLEEP_SEC = 2
PER_PAGE_TRANSITION_POST_SLEEP_SEC = 10
PER_PRODUCT_TRANSITION_PRE_SLEEP_SEC = 2
PER_PRODUCT_TRANSITION_POST_SLEEP_SEC = 2
PER_PRODUCT_PRE_BACK_SLEEP_SEC = 2
PER_PRODUCT_POST_BACK_SLEEP_SEC = 2

headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36'
}


class BaseDriver:
    start_url = None
    page_number = None
    driver = None
    upto = None
    source = None
    url_path = None
    data_path = None

    def __init__(self, start_url, upto, source):
        self.start_url = start_url
        self.page_number = 1
        self.upto = upto
        self.source = source
        current_file_path = os.path.dirname(__file__)
        self.url_path = os.path.join(current_file_path, '../data/' + source + '.txt')
        self.data_path = os.path.join(current_file_path, '../data/' + source + '/')
        if os.path.exists(self.url_path) or any(File.endswith(".html") for File in os.listdir(self.data_path)):
            print 'Source: ' + source + 'Files not clean!!!! Terminating with no change.'
            exit(1)
        urlopen = urllib2.urlopen(self.get_request(self.start_url)).read()
        self.driver = BeautifulSoup(urlopen, 'html.parser')

    def get_request(self, url):
        return urllib2.Request(url, headers=headers)

    def generate(self):
        while self.page_number <= self.upto:
            time.sleep(PER_PAGE_TRANSITION_PRE_SLEEP_SEC)
            self.extract_product_htmls()
            self.move_to_next_page()
            time.sleep(PER_PAGE_TRANSITION_POST_SLEEP_SEC + randint(1, 4))
            print str(self.page_number)
            self.page_number += 1

    def move_to_next_page(self):
        pass

    def extract_product_htmls(self):
        # div in srcOne and div in srcOne
        product_slots = None
        if self.source == 'srcOne':
            product_list = self.driver.find_all('div', class_=self.get_product_list_class())
            product_slots = []
            for product_list_entry in product_list:
                product_slots.extend(product_list_entry.find_all('li', class_=self.get_product_slot_class_name()))
        elif self.source == 'srcTwo':
            product_list = self.driver.find('section', id=self.get_product_list_id())
            product_slots = product_list.find_all('div', class_=self.get_product_slot_class_name())
        slot_number = 0
        with codecs.open(self.url_path, mode="a", encoding='utf-8') as url_file:
            for product in product_slots:
                book_page_url = self.get_product_url(product)
                # print book_page_url
                # self.switch_to_product_info()
                # html_file_name = self.data_path + str(self.page_number) + '_' + str(slot_number) + '_1.html'
                html_file_name = self.data_path + str(self.page_number) + '_' + str(slot_number) + '_2.html'
                # html_file_name = self.data_path + str(self.page_number) + '_' + str(slot_number) + '_3.html'
                with open(html_file_name, 'w') as html_file:
                    product_page_html = urllib2.urlopen(self.get_request(book_page_url)).read()
                    html_file.write(product_page_html)
                    url_file.write(book_page_url)
                    url_file.write("\n")
                time.sleep(PER_PRODUCT_POST_BACK_SLEEP_SEC + randint(1, 3))
                slot_number += 1

    def close(self):
        self.driver.close()

    def get_product_list_id(self):
        raise NotImplementedError("error message")

    def get_product_slot_class_name(self):
        raise NotImplementedError("error message")

    def get_title_href_class_name(self):
        raise NotImplementedError("error message")

    def get_product_url(self, product):
        raise NotImplementedError("error message")

    def get_product_list_class(self):
        raise NotImplementedError("error message")


class AmazonDriver(BaseDriver):
    def __init__(self, upto=100):
        # start_url = 'https://www.amazon.com/b/ref=s9_acsd_hfnv_hd_bw_bS_ct_x_ct08_w?_encoding=UTF8&node=17418&pf_rd_m=ATVPDKIKX0DER&pf_rd_s=merchandised-search-4&pf_rd_r=PJEPC4FWC7G0BJ98FF81&pf_rd_t=101&pf_rd_p=2d37c348-a4f3-588b-8aac-9a5e62da1b88&pf_rd_i=28'
        start_url = 'https://www.amazon.com/s/ref=lp_283155_nr_n_2?fst=as%3Aoff&rh=n%3A283155%2Cn%3A%211000%2Cn%3A3&bbn=1000&ie=UTF8&qid=1521838478&rnid=1000'
        # start_url = 'https://www.amazon.com/s/ref=lp_283155_nr_n_23?fst=as%3Aoff&rh=n%3A283155%2Cn%3A%211000%2Cn%3A22&bbn=1000&ie=UTF8&qid=1521838508&rnid=1000'
        BaseDriver.__init__(self, start_url, upto, 'srcOne')

    def get_product_slot_class_name(self):
        return "s-result-item"

    def get_title_href_class_name(self):
        # return "a-link-normal s-access-detail-page  s-color-twister-title-link a-text-normal"
        return "s-color-twister-title-link"

    def move_to_next_page(self):
        page_elems = self.driver.find('div', id='pagn')
        next_pages = page_elems.find_all('span', class_='pagnLink')
        for next_page in next_pages:
            a_next_page = next_page.find('a', href=True)
            if a_next_page.get_text() == str(self.page_number + 1):
                next_page_url = a_next_page['href']
                print next_page_url
                urlopen = urllib2.urlopen(self.get_request('https://www.amazon.com/' + next_page_url)).read()
                self.driver = BeautifulSoup(urlopen, 'html.parser')
                break

    def get_next_page_text(self):
        return str(self.page_number + 1)

    def get_next_page_elements_xpath(self):
        return "//div[@id='pagn']"

    def get_product_url(self, product):
        a = product.find('a', class_='s-access-detail-page', href=True)
        return a['href']

    def get_product_list_class(self):
        return 's-result-list-parent-container'


class BarnesAndNobleDriver(BaseDriver):
    def __init__(self, upto=102):
        # start_url = 'https://www.barnesandnoble.com/b/books/teens/biography-teens/_/N-29Z8q8Z1a6x'
        start_url = 'https://www.barnesandnoble.com/b/books/teens/business-teens/_/N-29Z8q8Z19yf'
        # start_url = 'https://www.barnesandnoble.com/b/books/teens/religion-teens/_/N-29Z8q8Z19uk'
        BaseDriver.__init__(self, start_url, upto, 'srcTwo')

    def move_to_next_page(self):
        next_page_url = self.start_url + "?Nrpp=20&page=" + str(self.page_number + 1)
        urlopen = urllib2.urlopen(self.get_request(self.start_url)).read()
        self.driver = BeautifulSoup(urlopen, 'html.parser')

    def get_product_list_id(self):
        return "gridView"

    def get_product_slot_class_name(self):
        return "product-shelf-tile-book"

    def get_title_href_class_name(self):
        return "product-info-title"

    def get_product_url(self, product):
        a = product.find('div', class_=self.get_title_href_class_name()).find('a', href=True)
        return 'https://www.barnesandnoble.com/' + a['href']


if __name__ == '__main__':
    cmd_args = sys.argv
    if len(cmd_args) != 2:
        print 'Expects command line argument as 1 or 2'
    driver = None
    if cmd_args[1] == "1":
        driver = AmazonDriver()
    elif cmd_args[1] == "2":
        driver = BarnesAndNobleDriver()
    else:
        print 'Expected command line argument to be 1 or 2. Found: ' + cmd_args[1]
    driver.generate()
