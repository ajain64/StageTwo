from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
import time
import glob
import os
import sys
import codecs

IMPLICIT_WAIT_TIME = "30000"

PAGE_LOAD_TIMEOUT_SEC = 30

PER_PAGE_TRANSITION_PRE_SLEEP_SEC = 2
PER_PAGE_TRANSITION_POST_SLEEP_SEC = 2
PER_PRODUCT_TRANSITION_PRE_SLEEP_SEC = 2
PER_PRODUCT_TRANSITION_POST_SLEEP_SEC = 2
PER_PRODUCT_PRE_BACK_SLEEP_SEC = 2
PER_PRODUCT_POST_BACK_SLEEP_SEC = 2


class BaseDriver:
    start_url = None
    page_number = None
    driver = None
    upto = None
    source = None
    url_path = None
    data_path = None
    options = Options()

    # options.add_argument("--headless")

    def __init__(self, start_url, upto, source):
        self.start_url = start_url
        self.page_number = 1
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(IMPLICIT_WAIT_TIME)
        self.driver.set_page_load_timeout(PAGE_LOAD_TIMEOUT_SEC)
        self.upto = upto
        self.source = source
        current_file_path = os.path.dirname(__file__)
        self.url_path = os.path.join(current_file_path, '../data/' + source + '.txt')
        self.data_path = os.path.join(current_file_path, '../data/' + source + '/')
        if os.path.exists(self.url_path) or any(File.endswith(".html") for File in os.listdir(self.data_path)):
            print 'Source: ' + source + 'Files not clean!!!! Terminating with no change.'
            exit(1)
        self.driver.get(self.start_url)
        time.sleep(1)
        while self.driver.execute_script('return document.readyState;') != 'complete':
            time.sleep(1)

    def generate(self):
        while self.page_number <= self.upto:
            time.sleep(PER_PAGE_TRANSITION_PRE_SLEEP_SEC)
            self.extract_product_htmls()
            self.move_to_next_page()
            time.sleep(PER_PAGE_TRANSITION_POST_SLEEP_SEC)
            print str(self.page_number)
            self.page_number += 1

    def move_to_next_page(self):
        pass

    def extract_product_htmls(self):
        time.sleep(5)
        product_list = self.driver.find_element_by_id(self.get_product_list_id())
        product_slots = product_list.find_elements_by_class_name(self.get_product_slot_class_name())
        slot_number = 0
        main_window = self.driver.current_window_handle
        with codecs.open(self.url_path, mode="a", encoding='utf-8') as url_file:
            for product in product_slots:
                actions = ActionChains(self.driver)
                product = product.find_element_by_class_name(self.get_title_href_class_name())
                time.sleep(PER_PRODUCT_TRANSITION_PRE_SLEEP_SEC)
                actions.key_down(Keys.COMMAND).click(product).key_up(Keys.COMMAND).perform()
                self.driver.switch_to.window(self.driver.window_handles[-1])
                # time.sleep(PER_PRODUCT_TRANSITION_POST_SLEEP_SEC)
                while self.driver.execute_script('return document.readyState;') != 'complete':
                    time.sleep(1)
                # self.switch_to_product_info()
                html_file_name = self.data_path + str(self.page_number) + '_' + str(slot_number) + '_3.html'
                with codecs.open(html_file_name, mode="a", encoding='utf-8') as html_file:
                    product_page_html = self.driver.page_source
                    html_file.write(product_page_html)
                    book_page_url = self.driver.current_url
                    url_file.write(book_page_url)
                    url_file.write("\n")
                self.driver.close()
                self.driver.switch_to.window(main_window)
                # time.sleep(PER_PRODUCT_POST_BACK_SLEEP_SEC)
                slot_number += 1

    def close(self):
        self.driver.close()

    def get_product_list_id(self):
        raise NotImplementedError("error message")

    def get_product_slot_class_name(self):
        raise NotImplementedError("error message")

    def get_title_href_class_name(self):
        raise NotImplementedError("error message")


class AmazonDriver(BaseDriver):
    def __init__(self, upto=100):
        start_url = 'https://www.amazon.com/b/ref=s9_acsd_hfnv_hd_bw_bS_ct_x_ct08_w?_encoding=UTF8&node=17418&pf_rd_m=ATVPDKIKX0DER&pf_rd_s=merchandised-search-4&pf_rd_r=PJEPC4FWC7G0BJ98FF81&pf_rd_t=101&pf_rd_p=2d37c348-a4f3-588b-8aac-9a5e62da1b88&pf_rd_i=28'
        BaseDriver.__init__(self, start_url, upto, 'srcOne')

    def get_product_list_id(self):
        if self.page_number == 1:
            return "mainResults"
        else:
            return "atfResults"

    def get_product_slot_class_name(self):
        return "s-result-item"

    def get_title_href_class_name(self):
        # return "a-link-normal s-access-detail-page  s-color-twister-title-link a-text-normal"
        return "s-color-twister-title-link"

    def move_to_next_page(self):
        page_elems = self.driver.find_element_by_xpath(self.get_next_page_elements_xpath())
        next_page = page_elems.find_element_by_link_text(self.get_next_page_text())
        # noinspection PyStatementEffect
        next_page.location_once_scrolled_into_view
        next_page.click()

    def get_next_page_text(self):
        return str(self.page_number + 1)

    def get_next_page_elements_xpath(self):
        return "//div[@id='pagn']"


class BarnesAndNobleDriver(BaseDriver):
    def __init__(self, upto=133):
        start_url = 'https://www.barnesandnoble.com/b/books/teens/biography-teens/_/N-29Z8q8Z1a6x'
        BaseDriver.__init__(self, start_url, upto, 'srcTwo')

    def move_to_next_page(self):
        next_page_url = self.start_url + "?Nrpp=20&page=" + str(self.page_number + 1)
        self.driver.get(next_page_url)

    def get_next_page_elements_xpath(self):
        return "//*[@id='searchGrid']/div/section[3]/ul"

    def get_product_list_id(self):
        return "gridView"

    def get_product_slot_class_name(self):
        # return "product-shelf-tile product-shelf-tile-book columns-5"
        return "product-shelf-tile-book"

    def get_title_href_class_name(self):
        return "product-info-title"
        # return "product-shelf-title product-info-title pt-xs"


if __name__ == '__main__':
    cmd_args = sys.argv
    if len(cmd_args) != 2:
        print 'Expects command line argument as 1 or 2'
    driver = None
    if cmd_args[1] == "1":
        driver = AmazonDriver()
    elif cmd_args[1] == "2":
        driver = BarnesAndNobleDriver()
    else:
        print 'Expected command line argument to be 1 or 2. Found: ' + cmd_args[1]
    driver.generate()
