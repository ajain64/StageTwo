CS 839 (Stage Two)
========================

Setup:
    - run make to fetch all dependencies

    - get nosetests (should be done via make but OSX has few issues and is recommended to install via easy_install
    instead of pip. Follow: https://stackoverflow.com/questions/32546228/installed-nose-but-cannot-use-on-command-line)

    - [OPTIONAL] run python setup.py install to install the repo in your path

    - to run test cases: type make test

Scrapping HTML data:

    * Url Based Scrapper:
        - Scrapes data only on the basis of sending urllib requests. 
        - Fast but easy to be marked as bot.
    * Selenium Based Scrapper: 
        - Hard to detect this as a bot. Simulates click on browser. 
        - Slower than Url based since all the scripts that executes on a
        browser will be executed. 
        - Headless browser.

Parsers:
        - Reads the files generated by scrapper and generates the required csv.